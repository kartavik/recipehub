<?php

namespace App\Http\Controllers;

use App\Ingredient;
use App\Measure;
use App\Recipe;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('dashboard.index');
    }

    public function recipes(int $count = 10)
    {
        return view('dashboard.recipes', [
            'recipes' => Recipe::with('categories')->paginate($count)
        ]);
    }

    public function ingredients()
    {
        return view('dashboard.ingredients', [
            'ingredients' => Ingredient::with('measure')->get()->all()
        ]);
    }

    public function measures(int $count = 10)
    {
        return view('dashboard.measures', [
            'measures' => Measure::query()->paginate($count)
        ]);
    }
}
