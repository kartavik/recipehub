<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteIngredient;
use App\Http\Requests\StoreIngredient;
use App\Ingredient;
use App\Measure;
use Illuminate\Support\Facades\Validator;

class IngredientController extends Controller
{
    public function list()
    {
        return Ingredient::with('measure')->get()->all();
    }

    public function redirectBack()
    {
        return redirect()->back();
    }

    public function store(StoreIngredient $request)
    {
        Ingredient::create($request->validated());

        return $this->redirectBack()->with('success', 'Ингредиент успешно добавлен!');
    }

    public function edit(Ingredient $ingredient)
    {
        return view('dashboard.ingredient.edit', [
            'ingredient' => $ingredient
        ]);
    }

    public function create()
    {
        return view('dashboard.ingredient.create', [
            'measures' => Measure::all()
        ]);
    }

    /**
     * @param StoreIngredient $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StoreIngredient $request)
    {
        Ingredient::updateOrCreate($request->validated());

        return redirect()->back();
    }

    public function remove(Ingredient $ingredient)
    {
        if ($ingredient->recipeCompositions()->count() > 0) {
            return redirect()
                ->back()
                ->withErrors(['warning' => 'Ингредиент используется в рецептах']);
        }

        $ingredient->delete();

        return redirect()->back()->with('success', 'Ингредиент успешно удален');
    }
}
