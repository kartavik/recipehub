<?php

namespace App\Http\Controllers;

use App\Measure;
use Illuminate\Http\Request;

class MeasureController extends Controller
{
    public function list()
    {
        return Measure::get()->all();
    }

    public function store(Request $request)
    {
        Measure::create($request->all());

        return redirect()
            ->back();
    }

    public function remove(Measure $measure)
    {
        $errors = [];

        if (($ingredientsCount = $measure->ingredients()->count()) > 0) {
            $errors = [
                'warning' => 'Невозможно удалить мерность => существуют ингредиенты, использующие её'
            ];
        } else {
            $measure->delete();
        }

        return redirect()
            ->back()
            ->withErrors($errors);
    }
}
