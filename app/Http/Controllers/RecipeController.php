<?php

namespace App\Http\Controllers;

use App\Category;
use App\Ingredient;
use App\Measure;
use App\Recipe;
use App\RecipeCategory;
use App\RecipeComposition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RecipeController extends Controller
{
    public function show(Recipe $recipe)
    {
        return view('dashboard.recipe.view', [
            'recipe' => $recipe,
        ]);
    }

    public function create()
    {
        return view('dashboard.recipe.create', [
            'measures' => Measure::all(),
            'ingredients' => Ingredient::with('measure')->get()->all(),
            'categories' => Category::all(),
        ]);
    }

    public function update(Request $request)
    {
        if ($request->has('ingredient')) {
            foreach ($request->get('ingredient') as $ingredient) {
                RecipeComposition::query()
                    ->where([
                        'recipe_id' => $request->get('recipe_id'),
                        'ingredient_id' => $ingredient['id']
                    ])
                    ->update([
                        'count' => $ingredient['count']
                    ]);
            }
        }

        return redirect()->back()->with('success', 'Рецепт успешно обновлен');
    }

    public function instantiate(Request $request)
    {
        $name = $request->get('name');
        $description = $request->get('description');
        $categories = $request->get('category_id') ?? [];
        $ingredients = $request->get('ingredient');
        $validator = Validator::make(
            [
                'name' => $name,
                'description' => $description,
                'categories' => $categories,
                'ingredients' => $ingredients,
            ],
            [
                'name' => 'unique:recipe',
                'description' => 'min:5',
                'categories' => 'exists:category,id',
                'ingredients.ingredient_id' => 'exists:ingredient,id',
                'ingredient.count' => 'min:1'
            ]
        );

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput($request->all());
        }

        $recipe = Recipe::create([
            'name' => $name,
            'description' => $description,
            'author_id' => $request->user()->id,
        ]);

        foreach ($ingredients as $ingredient) {
            RecipeComposition::create([
                'recipe_id' => $recipe->id,
                'ingredient_id' => $ingredient['ingredient_id'],
                'count' => $ingredient['count'],
            ]);
        }

        foreach ($categories as $category) {
            RecipeCategory::create([
                'recipe_id' => $recipe->id,
                'category_id' => $category
            ]);
        }

        return redirect()
            ->to(route('dashboard.recipes'))
            ->with('success', 'Рецепт успешно сохранен!');
    }

    public function destroy(Recipe $recipe)
    {
        RecipeCategory::query()
            ->where(['recipe_id' => $recipe->id])
            ->delete();

        RecipeComposition::query()
            ->where(['recipe_id' => $recipe->id])
            ->delete();

        $recipe->delete();

        return redirect()->back()->with('success', 'Рецепт успешно удален');
    }
}
