<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\StoreCategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(int $count = 10)
    {
        return view('dashboard.categories', [
            'categories' => Category::with('parent')->paginate($count)
        ]);
    }

    public function create()
    {
        return view('dashboard.category.create', [
            'categories' => Category::all()
        ]);
    }

    public function store(StoreCategory $request)
    {
        Category::create([
            'name' => $request->get('name'),
            'parent_id' => ($id = $request->get('parent_id')) ? $id : null
        ]);

        return redirect(route('dashboard.categories.index'));
    }

    public function destroy(Category $category)
    {
        $errors = [];

        if ($category->children()->count() > 0) {
            $errors['children'] = 'Категория содержит подкатегории';
        }

        if ($category->recipes()->count() > 0) {
            $errors['recipes'] = 'Категорию используют в некоторых рецептах';
        }

        if (empty($errors)) {
            $category->delete();
        }

        return redirect()
            ->back()
            ->withErrors($errors);
    }
}
