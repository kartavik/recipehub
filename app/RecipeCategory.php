<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\RecipeCategory
 *
 * @property-read \App\Category $category
 * @property-read \App\Recipe $recipe
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeCategory query()
 * @mixin \Eloquent
 */
class RecipeCategory extends Model
{
    protected $table = 'recipe_category';

    protected $fillable = [
        "recipe_id",
        "category_id"
    ];

    public function category(): HasOne
    {
        return $this->hasOne(Category::class, "category_id", "id");
    }

    public function recipe(): HasOne
    {
        return $this->hasOne(Recipe::class, "recipe_id", "id");
    }
}
