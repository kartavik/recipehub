<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\RecipeComposition
 *
 * @property-read \App\Ingredient $ingredient
 * @property-read \App\Recipe $recipe
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeComposition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeComposition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RecipeComposition query()
 * @mixin \Eloquent
 */
class RecipeComposition extends Model
{
    protected $table = 'recipe_composition';

    protected $fillable = [
        "recipe_id",
        "ingredient_id",
        "count"
    ];

    public function recipe(): HasOne
    {
        return $this->hasOne(Recipe::class, "recipe_id", "id");
    }

    public function ingredient(): HasOne
    {
        return $this->hasOne(Ingredient::class, "id", "ingredient_id");
    }
}
