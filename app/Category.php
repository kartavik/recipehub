<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Category
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Category[] $children
 * @property-read \App\Category $parent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category query()
 * @mixin \Eloquent
 */
class Category extends Model
{
    protected $table = 'category';

    protected $fillable = [
        "name",
        "parent_id"
    ];

    public function parent(): HasOne
    {
        return $this->hasOne(Category::class, "id", "parent_id");
    }

    public function children(): HasMany
    {
        return $this->hasMany(Category::class, "parent_id", "id");
    }

    public function recipes(): HasManyThrough
    {
        return $this->hasManyThrough(Recipe::class, RecipeCategory::class, "category_id", "id", "id", "recipe_id");
    }
}
