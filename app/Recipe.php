<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Recipe
 *
 * @property-read \App\User $author
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Category[] $categories
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\RecipeComposition[] $compositions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Recipe query()
 * @mixin \Eloquent
 */
class Recipe extends Model
{
    protected $table = 'recipe';

    protected $fillable = [
        "name",
        "description",
        "author_id",
    ];

    public function author(): HasOne
    {
        return $this->hasOne(User::class, "author_id", "id");
    }

    public function ingredients()
    {
        return $this->hasMany(RecipeComposition::class, 'recipe_id', 'id')->with('ingredient');
    }

    public function categories(): HasManyThrough
    {
        return $this->hasManyThrough(Category::class, RecipeCategory::class, "recipe_id", "id", "id", "category_id");
    }
}
