<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Measure
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Ingredient[] $ingredients
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Measure newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Measure newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Measure query()
 * @mixin \Eloquent
 */
class Measure extends Model
{
    protected $table = 'measure';

    protected $fillable = [
        "name",
        "postfix",
    ];

    public function ingredients(): HasMany
    {
        return $this->hasMany(Ingredient::class, "measure_id", "id");
    }
}
