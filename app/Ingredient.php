<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Ingredient
 *
 * @property-read \App\Measure $measure
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\RecipeComposition[] $recipeCompositions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ingredient newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ingredient newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ingredient query()
 * @mixin \Eloquent
 */
class Ingredient extends Model
{
    protected $with = [
        'measure'
    ];

    protected $table = 'ingredient';

    protected $fillable = [
        "name",
        "measure_id",
        "count"
    ];

    public function measure(): HasOne
    {
        return $this->hasOne(Measure::class, "id", "measure_id");
    }

    public function recipeCompositions(): HasMany
    {
        return $this->hasMany(RecipeComposition::class, "ingredient_id", "id");
    }
}
