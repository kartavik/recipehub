@extends("layouts.app")

@section('content-wrapper')
    <div class="container-fluid h-100">
        <div class="row h-100">
            <div id="sidebar" class="col-2 border-right h-100 shadow-sm">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link text-dark" href="{{ route('dashboard.recipes') }}">
                            <i class="fas fa-book"></i>
                            <span>Мои рецепты</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark" href="{{ route("dashboard.ingredients") }}">
                            <i class="fas fa-puzzle-piece"></i>
                            <span>Ингредиенты</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark" href="{{ route('dashboard.measures') }}">
                            <i class="fas fa-weight-hanging"></i>
                            <span>Мерности</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark" href="{{ route('dashboard.categories.index') }}">
                            <i class="fas fa-align-justify"></i>
                            <span>Категории</span>
                        </a>
                    </li>
                </ul>
                <div class="nav-item">
                </div>
            </div>
            <div id="content" class="col-10 h-100 fixed-overflow">
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show m-1 p-1 rounded-0" role="alert">
                        {{ Session::get('success') }}
                        <button type="button" class="close p-1" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                @error('warning')
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    {{ $message }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @enderror

                @yield('content')
            </div>
        </div>
    </div>
@endsection
