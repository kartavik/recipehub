@extends('layouts.dashboard')

@section('content')
    <div class="container m-2">
        <h2 class="m-1">Добавление категории</h2>
        <form method="POST" action="{{ route('dashboard.categories.store') }}">
            @csrf

            <div class="container-fluid border-bottom">
                <div class="form-group row">
                    <label for="name" class="col-form-label text-md-right">Название</label>
                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                               name="name"
                               value="{{ old('name') }}" required autocomplete="category" autofocus>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="parent_id" class="col-form-label text-md-right">Родительская категория</label>
                    <div class="col-md-6">
                        <select id="parent_id" type="text" class="form-control @error('parent_id') is-invalid @enderror"
                                name="parent_id">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>

                        @error('parent_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="container-fluid d-flex justify-content-end">
                <button type="submit" class="btn btn-outline-success m-2">Сохранить</button>
            </div>
        </form>
    </div>
@endsection
