@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid justify-content-between d-flex">
        <h3 class="m-1">Мои ингредиенты</h3>
        <a href="{{ route('dashboard.ingredient.create') }}" role="button" class="btn btn-sm btn-primary rounded-0 p-1 m-1">
            Добавить ингредиент
        </a>
    </div>
    <table class="table table-sm">
        <thead>
        <tr>
            <th>#</th>
            <th>Название</th>
            <th>Мерность</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        @foreach($ingredients as $ingredient)
            <tr>
                <td>{{ $ingredient->id }}</td>
                <td>{{ $ingredient->name }}</td>
                <td>{{ $ingredient->measure->name }} ({{ $ingredient->measure->postfix }})</td>
                <td>
                    <div class="container-fluid row">
                        <form method="POST"
                              action="{{ route('dashboard.ingredient.remove', $ingredient->id) }}">
                            @csrf
                            @method('DELETE')

                            <button class="btn btn-sm btn-outline-danger m-1">
                                <i class="fas fa-times"></i>
                            </button>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
