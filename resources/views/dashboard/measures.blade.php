@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid">
        <h3 class="m-1">Мои мерности</h3>
    </div>
    <div class="row">
        <div class="col-6">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th>Постфикс</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                @foreach($measures as $measure)
                    <tr>
                        <td>{{ $measure->id }}</td>
                        <td>{{ $measure->name }}</td>
                        <td>{{ $measure->postfix }}</td>
                        <td>
                            <form method="POST" action="{{ route('dashboard.measure.remove', $measure->id) }}">
                                @csrf
                                @method('DELETE')

                                <button class="btn btn-sm btn-outline-danger" type="submit">
                                    <i class="fas fa-times"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div>
            <form method="POST" action="{{ route('dashboard.measure.store') }}">
                @csrf

                <div class="form-group">
                    <label for="name">Название</label>
                    <input id="name" name="name" type="text" class="form-control">
                    <label for="postfix">Постфикс</label>
                    <input id="postfix" name="postfix" type="text" class="form-control">

                </div>
                <div class="form-group">
                    <button class="btn btn-outline-success" type="submit">Добавить</button>
                </div>
            </form>
        </div>
    </div>
@endsection
