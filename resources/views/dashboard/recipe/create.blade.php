@extends('layouts.dashboard')

@section('content')
    <div class="container m-2">
        <h2 class="m-1">Добавление рецепта</h2>
        @error('ingredient')
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ $message }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @enderror
        <form method="POST" action="{{ route('dashboard.recipe.instantiate') }}">
            @csrf
            @method('PUT')

            <div class="container border-bottom">
                <div class="form-group row">
                    <label for="recipe-name" class=" col-form-label text-md-right">Название</label>
                    <div class="col-md-6">
                        <input id="recipe-name" type="text"
                               class="form-control @error('recipe-name') is-invalid @enderror"
                               name="name"
                               value="{{ old('recipe-name') }}" required autofocus>
                        @error('recipe-name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-form-label text-md-right">Описание</label>
                    <div class="col-md-6">
                    <textarea rows="3" id="description" type="text"
                              class="form-control @error('description') is-invalid @enderror"
                              name="description">
                        {{ old('description') }}
                    </textarea>
                        @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>

            <h2 class="m-1">Категории</h2>
            <div class="container border-bottom">
                <div id="recipe-categories" class="container-fluid row">
                </div>
                <div class="container-fluid d-flex justify-content-between">
                    <button id="append-category" class="btn btn-sm btn-dark m-2 rounded-0" type="button">
                        добавить
                    </button>
                </div>
            </div>

            <h2 class="m-1">Ингредиенты</h2>
            <div class="container border-bottom">
                <div id="recipe-ingredients">
                </div>
                <div class="container-fluid d-flex justify-content-between">
                    <button id="append-ingredient" class="btn btn-sm btn-dark m-2 rounded-0" type="button">
                        добавить
                    </button>
                    <div>
                        <span class="text-info">Нет в списке?</span>
                        <button data-toggle="modal" data-target="#ingredientCreatorModal" id="create-ingredient"
                                type="button"
                                class="btn btn-sm btn-outline-secondary m-2 rounded-0">Создать новый ингредиент
                        </button>
                    </div>
                </div>
            </div>

            <div class="container d-flex justify-content-end">
                <button class="btn btn-outline-success col-2 m-2 rounded-0" type="submit">Сохранить</button>
            </div>
        </form>
    </div>
    <script>
        $(document).ready(function () {
            let ingredients = @json($ingredients);
            let categories = @json($categories);
            let count = 0;

            $("#append-ingredient").on('click', function () {
                let ingredient = $("<div></div>");
                let select = $(`<select name="ingredient[${count}][ingredient_id]" class="form-control col m-1"></select>`);

                ingredient.attr("class", "form-group row");
                Array.from(ingredients).forEach(function (item) {
                    select.append($(`<option value="${item.id}">${item.name} (${item.measure.postfix})</option>`))
                });
                ingredient.append(select);
                ingredient.append(
                    $(`<label for="$ingredient-${count}" class="col-form-label m-1">Количество:</label>`),
                    $(`<input id="ingredient-${count}" name="ingredient[${count}][count]" class="form-control col-1 m-1" type="number">`)
                );

                count = count + 1;
                $("#recipe-ingredients").append(ingredient);
            });

            $("#append-category").on('click', function () {
                let category = $('<div class="col"></div>');
                let select = $(`<select name="category_id[]" class="form-control-sm m-1"></select>`);

                Array.from(categories).forEach(function (item) {
                    select.append($(`<option value="${item.id}">${item.name}</option>`))
                });
                category.append(select);

                count = count + 1;
                $("#recipe-categories").append(category);
            });
        });
    </script>

    <div class="modal" id="ingredientCreatorModal" tabindex="-1" role="dialog">
        <form method="POST" action="{{ route('dashboard.ingredient.store') }}">
            @csrf

            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Добавление игредиента</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <label for="name">Ингредиент</label>
                        <input id="name" class="form-control" type="text" name="name">
                        <label for="measure_id">Мерность</label>

                        <div class="form-group">
                            <select id="measure_id" class="form-control" name="measure_id">
                                @foreach($measures as $measure)
                                    <option value="{{ $measure->id }}">
                                        {{ $measure->name }}, {{ $measure->postfix }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" onclick="">Сохранить</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
