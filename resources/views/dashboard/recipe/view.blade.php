@extends('layouts.dashboard')

@section('content')
    <form method="POST" action="{{ route('dashboard.recipe.update', ['recipe_id' => $recipe->id]) }}">
        @csrf
        <div class="container-fluid border-bottom">
            <h2>{{ $recipe->name }}</h2>
        </div>
        <div class="container-fluid border-bottom">
            <h3>Описание</h3>
            {{ $recipe->description }}
        </div>
        <div class="container-fluid border-bottom">
            <h3>Категории</h3>
            @foreach($recipe->categories as $category)
                <span class="text-info">{{ $category->name }}; </span>
            @endforeach
        </div>
        <div class="container-fluid border-bottom">
            <h3>Игредиенты</h3>
            @foreach($recipe->ingredients as $key => $composition)
                <div class="form-group row m-1">
                    <label class="col-form-label"
                           for="ingredient-{{ $composition->ingredient->id }}">{{ $composition->ingredient->name }}
                        ({{ $composition->ingredient->measure->postfix }})</label>
                    <input type="hidden" name="ingredient[{{ $key }}][id]" value="{{ $composition->ingredient->id }}">
                    <input id="ingredient-{{ $composition->ingredient->id }}" value="{{ $composition->count }}"
                           type="number" name="ingredient[{{ $key }}][count]"
                           class="form-control form-control-sm m-1 col-1">
                </div>
            @endforeach
        </div>
        <div class="form-group">
            <button class="btn btn-success" type="submit">Обновить</button>
        </div>
    </form>
@endsection
