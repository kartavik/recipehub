@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid justify-content-between d-flex">
        <h3 class="m-1">Категории</h3>
        <a href="{{ route('dashboard.categories.create') }}" role="button"
           class="btn btn-sm btn-primary rounded-0 p-1 m-1">
            Добавить категорию
        </a>
    </div>
    @error('recipes')
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        {{ $message }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @enderror

    @error('children')
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        {{ $message }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @enderror
    <div class="container-fluid">
        <table class="table table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Название</th>
                <th>Родительская категория</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody style="height: 100px; overflow-y: scroll">
            @foreach($categories as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->name }}</td>
                    <td>{{ $category->parent->name ?? '' }}</td>
                    <td>
                        <form method="POST" action="{{ route('dashboard.categories.destroy', $category->id) }}">
                            @csrf
                            @method('DELETE')

                            <button class="btn btn-sm btn-outline-danger" type="submit">
                                <i class="fas fa-times"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $categories->links('vendor.pagination.bootstrap-4') }}
    </div>
@endsection
