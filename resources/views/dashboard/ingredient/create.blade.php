@extends('layouts.dashboard')

@section('content')
    <div class="col-4 m-2">
        <h2>Добавление ингредиента</h2>
        <form method="POST" action="{{ route('dashboard.ingredient.store') }}">
            @csrf

            <div class="form-group">
                <label for="name">Название</label>
                <input id="name" name="name" class="form-control" type="text" required autofocus>
            </div>
            <div class="form-group">
                <label for="measure_id">Мерность</label>
                <select name="measure_id" class="form-control" id="measure_id">
                    @foreach($measures as $measure)
                        <option value="{{ $measure->id }}">{{ $measure->name }} ({{ $measure->postfix }})</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <button class="btn btn-success rounded-0" type="submit">Сохранить</button>
            </div>
        </form>
    </div>
@endsection
