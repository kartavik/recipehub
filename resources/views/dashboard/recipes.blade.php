@extends('layouts.dashboard')

@section('content')
    <div class="container-fluid justify-content-between d-flex">
        <h3 class="m-1">Мои рецепты</h3>
        <a href="{{ route('dashboard.recipe.create') }}" role="button" class="btn btn-sm btn-primary rounded-0 p-1 m-1">
            Добавить рецепт
        </a>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Имя</th>
            <th>Описание</th>
            <th>Категории</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        @foreach($recipes as $recipe)
            <tr>
                <td>{{ $recipe->id }}</td>
                <td>{{ $recipe->name }}</td>
                <td>{{ $recipe->description }}</td>
                <td>
                    @foreach($recipe->categories as $category)
                        <span class="text-info">{{ $category->name }}</span><br>
                    @endforeach
                </td>
                <td class="row">
                    <a href="{{ route('dashboard.recipe.show', ['recipe' => $recipe->id]) }}"
                       role="button"
                       class="btn btn-sm btn-outline-secondary rounded-0 m-1">
                        <i class="far fa-eye"></i>
                    </a>
                    <form method="POST" action="{{ route('dashboard.recipe.destroy', $recipe->id) }}">
                        @csrf
                        @method('DELETE')
                        <button type="submit"
                                class="btn btn-sm btn-outline-danger rounded-0 m-1">
                            <i class="fa fa-times"></i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $recipes->links('vendor.pagination.bootstrap-4') }}
@endsection
