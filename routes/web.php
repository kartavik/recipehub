<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers;

Route::redirect('/', '/dashboard/recipes');

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::prefix('dashboard')
        ->name('dashboard.')
        ->group(function () {
            Route::get('/', [Controllers\DashboardController::class, "index"])->name('index');
            Route::get('/recipes', [Controllers\DashboardController::class, 'recipes'])->name('recipes');
            Route::get('/measures', [Controllers\DashboardController::class, 'measures'])->name('measures');
            Route::get('/ingredients', [Controllers\DashboardController::class, 'ingredients'])->name('ingredients');
            Route::prefix('recipe')
                ->name('recipe.')
                ->group(function () {
                    Route::get('/create', [Controllers\RecipeController::class, 'create'])->name('create');
                    Route::put('/instantiate', [Controllers\RecipeController::class, 'instantiate'])->name('instantiate');
                    Route::delete('/{recipe}/destroy', [Controllers\RecipeController::class, 'destroy'])->name('destroy');
                    Route::post('/update', [Controllers\RecipeController::class, 'update'])->name('update');
                    Route::get('/{recipe}', [Controllers\RecipeController::class, 'show'])->name('index');
                    Route::get('/{recipe}/view', [Controllers\RecipeController::class, 'show'])->name('show');
                    Route::get('/{recipe}/edit', [Controllers\RecipeController::class, 'edit'])->name('edit');
                });
            Route::prefix('measure')
                ->name('measure.')
                ->group(function () {
                    Route::post('/store', [Controllers\MeasureController::class, 'store'])->name('store');
                    Route::delete('/remove/{measure}', [Controllers\MeasureController::class, 'remove'])->name('remove');
                });
            Route::prefix('ingredient')
                ->name('ingredient.')
                ->group(function () {
                    Route::get('/edit/{ingredient}', [Controllers\IngredientController::class, 'edit'])->name('edit');
                    Route::get('/create', [Controllers\IngredientController::class, 'create'])->name('create');
                    Route::post('/store', [Controllers\IngredientController::class, 'store'])->name('store');
                    Route::delete('/remove/{ingredient}', [Controllers\IngredientController::class, 'remove'])->name('remove');
                    Route::post('/update/{ingredient}', [Controllers\IngredientController::class, 'update'])->name('update');
                });
            Route::resource('/categories', 'CategoryController');
        });
});
