<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipeCompositionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipe_composition', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("recipe_id")->unsigned();
            $table->integer("ingredient_id")->unsigned();
            $table->integer("count");
            $table->foreign("recipe_id", "composition_recipe_fk")
                ->references("id")
                ->on("recipe");
            $table->foreign("ingredient_id", "composition_ingredient_fk")
                ->references("id")
                ->on("ingredient");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipe_composition');
    }
}
