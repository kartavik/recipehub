<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipeCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipe_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("recipe_id")->unsigned();
            $table->integer("category_id")->unsigned();
            $table->foreign("recipe_id", "category_recipe_fk")
                ->references("id")
                ->on("recipe");
            $table->foreign("category_id", "recipe_category_fk")
                ->references("id")
                ->on("category");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipe_category');
    }
}
